
import numpy as np
import scipy.ndimage as nd

def project_vertex_signal(mesh, stack, centre=None):

    vertices = mesh.data['vertex']
    Nv = len(vertices)
    print 'Nv', Nv

    signal = np.zeros((Nv,), dtype=float)
    imax, jmax, kmax = stack.shape
    max_idx = np.array((imax-1, jmax-1, kmax-1))
    min_idx = np.array((0, 0, 0))

    if centre is None:
        centre = max_idx/2

    def clip(v):
        return np.maximum(min_idx, np.minimum(max_idx, v))

    h = 6
    d0 = 0
    d1 = -h
    ns = np.rint(abs(d1-d0))+1
    tt = np.linspace(0, 1, ns)
    op = np.max
    for i in range(Nv):
        if i%10000==0:
            print i
        v = vertices[i]
        x, y, z, nx, ny, nz = map(v.get, ['x', 'y', 'z', 'nx', 'ny', 'nz'])
        p = np.array((x, -y, z), dtype=float) + centre 
        n = np.array((nx, -ny, nz), dtype=float) # Outward pointing unit normal
        p_start = clip(p + d0*n)
        p_end = clip(p + d1*n)
        signal[i] = op( nd.map_coordinates(stack,
                            ([(p_start[0]*(1-t) + p_end[0]*t) for t in tt],
                             [(p_start[1]*(1-t) + p_end[1]*t) for t in tt],
                             [(p_start[2]*(1-t) + p_end[2]*t) for t in tt]),
                                                         order=1 ))
    return signal


def convert_signal_colour(mesh, signal):

    vertices = mesh.data['vertex']
    Nv = len(vertices)

    print signal
    smin = np.amin(signal)
    smax = np.amax(signal)

    for i in range(Nv):
        v = vertices[i]
        u = int(255*(signal[i]-smin)/(smax-smin))
        vertices[i]['red'] = u
        vertices[i]['green'] = u
        vertices[i]['blue'] = u
    descr = mesh.descr
    vertex_descr = next((d for d in descr if d[0]=='vertex'))
    vertex_descr[2].extend([('red', ['uchar']), ('green', ['uchar']), ('blue', ['uchar']) ])


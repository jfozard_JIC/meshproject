
import sys
from mesh import Mesh
from PIL import Image
from project import project_vertex_signal, convert_signal_colour
import numpy as np

def load_tiff(fn):
    im = Image.open(fn)
    frames = []
    i = 0
    try:
        while True:
            im.seek(i)
            frames.append(np.asarray(im))
            i += 1
    except EOFError:
        pass

    print len(frames), frames[-1].shape, frames[-1].dtype

    im = np.dstack(frames)
    del frames
    return im

m = Mesh.from_ply(sys.argv[1])
print m.descr

min_x = np.array((1000000., 1000000., 1000000.))

max_x = np.array((-100000., -100000., -100000.))

for v in m.data['vertex']:
    x = np.array((v['x'], v['y'], v['z']))
    min_x = np.minimum(x, min_x)
    max_x = np.maximum(x, max_x)

print min_x, max_x, max_x - min_x

s = load_tiff(sys.argv[2])

print s.shape

s = np.transpose(s, (1, 0, 2))
c = 0.5*np.array((222, 384, 512))

print s.shape, min_x+c, max_x+c, c - min_x, c - max_x

signal = project_vertex_signal(m, s, c)
convert_signal_colour(m, signal)
m.write_ply(sys.argv[3])

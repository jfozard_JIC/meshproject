
import numpy as np
from ply_parser import parse_ply, write_ply

class Mesh(object):
    def __init__(self):
        pass


    @classmethod
    def from_ply(cls, fn):
        descr, data = parse_ply(fn)
        m = Mesh()
        m.descr = descr
        m.data = data
        return m
    
    def write_ply(self, fn):
        write_ply(fn, self.descr, self.data)


